package cmd

import (
	"os"
	"testing"

	"0xacab.org/leap/menshen/storage"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestInviteTokenValidate(t *testing.T) {
	tokenOK := InviteToken{
		Key:     "1234",
		Buckets: []string{"bucket1", "bucket2"},
	}
	assert.NoError(t, tokenOK.Validate(), "This bucket should be valid")

	tokenInvalidKey := InviteToken{
		Key:     "1234 ",
		Buckets: []string{"bucket1", "bucket2"},
	}
	assert.Error(t, tokenInvalidKey.Validate(), "This bucket should fail because of the trailing space")

	tokenEmptyBuckets := InviteToken{
		Key:     "1234",
		Buckets: []string{},
	}
	require.Error(t, tokenEmptyBuckets.Validate(), "This bucket should fail because it has no buckets")

	tokenBucketWithSpace := InviteToken{
		Key:     "1234",
		Buckets: []string{" bucket1"},
	}
	require.Error(t, tokenBucketWithSpace.Validate(), "This bucket should fail because the bucket has a space")
}

func TestDBFunctions(t *testing.T) {
	dbFile := "/tmp/leap-invitectl.db"
	os.Remove(dbFile)

	db, err := storage.OpenDatabase(dbFile)
	require.NoError(t, err, "Could not open/create db")
	require.NotNil(t, db, "The db should not be empty")
	defer db.Close()

	// delete all invite tokens to have a clean state
	err = deleteAllInviteTokens(db)
	require.NoError(t, err, "Could not delete all invite tokens in db")

	// get all tokens and check list is empty
	tokens, err := getInviteTokens(db)
	require.NoError(t, err, "Could not get invite tokens")
	require.Equal(t, 0, len(tokens), "The list of tokens should be empty")

	// add two invite tokens
	testInviteToken1 := InviteToken{
		Key:     "1234567890",
		Buckets: []string{"bucket1"}}
	err = insertInviteToken(db, testInviteToken1)
	require.NoError(t, err, "Could not insert invite token 1")

	testInviteToken2 := InviteToken{
		Key:     "0987654321",
		Buckets: []string{"bucket1", "bucket2"}}
	err = insertInviteToken(db, testInviteToken2)
	require.NoError(t, err, "Could not insert invite token 2")

	testInviteToken3 := InviteToken{
		Key:     "045484378347",
		Buckets: []string{"bucket1", "bucket2", "bucket3"}}
	err = insertInviteToken(db, testInviteToken3)
	require.NoError(t, err, "Could not insert invite token 3")

	// check if they match
	tokens, err = getInviteTokens(db)
	require.NoError(t, err, "Could not get invite tokens")
	require.Equal(t, testInviteToken1, tokens[0], "The test invite token 1 does not match with the token of the db")
	require.Equal(t, testInviteToken2, tokens[1], "The test invite token 2 does not match with the token of the db")
	require.Equal(t, testInviteToken3, tokens[2], "The test invite token 3 does not match with the token of the db")

	// delete two of the three buckets
	deletedRows, err := deleteInviteTokenByBucket(db, "bucket2")
	require.NoError(t, err, "Could not delete by bucket")
	require.Equal(t, int64(2), deletedRows, "should only delete 2 invite token")

	// delete  the last existing one by token/key
	deletedRows, err = deleteInviteTokenByKey(db, "1234567890")
	require.NoError(t, err, "Could not delete by token")
	require.Equal(t, int64(1), deletedRows, "should only delete 2 invite token")

	os.Remove(dbFile)
}
