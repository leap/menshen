package cmd

import (
	"encoding/base64"
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/tj/assert"
)

func TestGenerateKey(t *testing.T) {
	prefix := "solitech__"
	keyLength := 16
	inviteToken, inviteTokenHashed, err := generateInviteToken(prefix, keyLength)
	require.NoError(t, err, "Could not generate invite token")
	require.NotEmpty(t, inviteToken, "Invite Token (plain) should not be empty")
	require.NotEmpty(t, inviteTokenHashed, "Invite Token (hashed) should not be empty")

	token, err := base64.StdEncoding.DecodeString(inviteTokenHashed)
	require.NoError(t, err, "Could not base64 decode invite token")
	require.NotEmpty(t, token, "Token should not be empty")
}

func TestHashInviteToken(t *testing.T) {
	inviteToken := "solitech_12345"
	inviteTokenHashed := hashInviteToken(inviteToken)
	assert.Equal(t, "4au3ZZadbFf6AGsh27fMoKY1g4y6EPWUZw7TVBbL4iA=", inviteTokenHashed)
}
