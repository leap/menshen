package api

import (
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"
	"time"

	m "0xacab.org/leap/menshen/pkg/models"
	"0xacab.org/leap/menshen/storage"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestBridgeFilters(t *testing.T) {
	type authTokenDbEntry struct {
		key     string
		buckets string
	}

	location1 := "New York"

	bridge1 := &m.Bridge{
		Host:      "bridge1",
		Port:      443,
		Transport: "TCP",
		Type:      "obfs4",
		Bucket:    "bucket1",
	}

	bridge2 := &m.Bridge{
		Host:      "bridge2",
		Port:      443,
		Transport: "TCP",
		Type:      "obfs4",
	}

	bridge3 := &m.Bridge{
		Host:           "bridge3",
		Port:           443,
		Transport:      "TCP",
		Type:           "obfs4",
		Bucket:         "bucket2",
		LastSeenMillis: time.Now().UnixMilli(),
	}

	testTable := []struct {
		name         string
		mockRegistry *registry
		expected     func(*registry) string
		authToken    string
		dbAuthTokens []authTokenDbEntry
	}{
		{"no auth token only private bridges",
			&registry{
				bridges: bridgeMap{location1: []*m.Bridge{bridge1}},
			},
			func(r *registry) string {
				return "[]\n"
			},
			"",
			[]authTokenDbEntry{{"key1", "bucket1"}},
		},
		{"auth token one private bridge",
			&registry{
				bridges: bridgeMap{location1: []*m.Bridge{bridge1}},
			},
			func(r *registry) string {
				bytes, err := json.Marshal(r.bridges[location1])
				assert.NoError(t, err)
				return string(bytes)
			},
			"key1",
			[]authTokenDbEntry{{"key1", "bucket1"}},
		},
		{"auth token one private one public bridge",
			&registry{
				bridges: bridgeMap{location1: []*m.Bridge{
					bridge1, bridge2,
				},
				},
			},
			func(r *registry) string {
				bytes, err := json.Marshal(r.bridges[location1])
				assert.NoError(t, err)
				return string(bytes)
			},
			"key1",
			[]authTokenDbEntry{{"key1", "bucket1"}},
		},
		{"auth token with multiple buckets two private bridges",
			&registry{
				bridges: bridgeMap{location1: []*m.Bridge{
					bridge1,
					bridge3,
				},
				},
			},
			func(r *registry) string {
				bytes, err := json.Marshal(r.bridges[location1])
				assert.NoError(t, err)
				return string(bytes)
			},
			"key1",
			[]authTokenDbEntry{{"key1", "bucket1,bucket2"}},
		},
		{"auth token with lastSeenCutoffMillis enabled",
			&registry{
				bridges: bridgeMap{location1: []*m.Bridge{
					bridge1,
					bridge3,
				},
				},
				// Cut off is 5 seconds
				lastSeenCutoffMillis: 5000,
			},
			func(r *registry) string {
				bytes, err := json.Marshal([]*m.Bridge{bridge3})
				assert.NoError(t, err)
				return string(bytes)
			},
			"key1",
			[]authTokenDbEntry{{"key1", "bucket1,bucket2"}},
		},
	}

	for _, tc := range testTable {
		t.Run(tc.name, func(t *testing.T) {

			dir, err := os.MkdirTemp("", "")
			assert.NoError(t, err)
			defer os.RemoveAll(dir)

			db, err := storage.OpenDatabase(dir + "/db1.sql")
			assert.NoError(t, err)
			defer db.Close()

			stmt, err := db.Prepare("INSERT INTO tokens(key, buckets) VALUES(?, ?)")
			assert.NoError(t, err)

			for _, token := range tc.dbAuthTokens {
				h := sha256.New()
				h.Write([]byte(token.key))
				authTokenHashBytes := h.Sum(nil)

				authTokenHashString := base64.StdEncoding.EncodeToString(authTokenHashBytes)
				_, err = stmt.Exec(authTokenHashString, token.buckets)
				assert.NoError(t, err)
			}

			expectedResponse := tc.expected(tc.mockRegistry)

			e := echo.New()
			e.Use(storageMiddleware(db))
			e.Use(authTokenMiddleware)
			e.GET("/api/5/bridges", tc.mockRegistry.ListAllBridges)
			e.GET("/api/5/bridge/:location", tc.mockRegistry.BridgePicker)

			// First test ListAllBridges
			req := httptest.NewRequest(http.MethodGet, "/api/5/bridges?type=obfs4", nil)
			req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
			if tc.authToken != "" {
				req.Header.Set("x-menshen-auth-token", tc.authToken)
			}
			rec := httptest.NewRecorder()

			e.ServeHTTP(rec, req)
			assert.Equal(t, http.StatusOK, rec.Code)
			assert.Equal(t, strings.TrimSpace(expectedResponse), strings.TrimSpace(rec.Body.String()))

			// The output from BridgePicker should be the same since we're just filtering by buckets
			req = httptest.NewRequest(http.MethodGet, fmt.Sprintf("/api/5/bridge/%v", url.PathEscape(location1)), nil)
			req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
			if tc.authToken != "" {
				req.Header.Set("x-menshen-auth-token", tc.authToken)
			}
			rec = httptest.NewRecorder()

			e.ServeHTTP(rec, req)
			assert.Equal(t, http.StatusOK, rec.Code)
			assert.Equal(t, strings.TrimSpace(expectedResponse), strings.TrimSpace(rec.Body.String()))
		})
	}
}
