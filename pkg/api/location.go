package api

import (
	"net/http"

	m "0xacab.org/leap/menshen/pkg/models"
	"github.com/labstack/echo/v4"
)

// ServiceInfo godoc
// @Summary      Get Service Info
// @Description  fetch service information, including location and common tunnel config
// @Tags         Provisioning
// @Accept       json
// @Produce      json
// @Success      200  {object}  models.EIPService
// @Failure      400  {object}  error
// @Failure      404  {object}  error
// @Failure      500  {object}  error
// @Router       /api/5/service [get]
func (r *registry) ServiceInfo(c echo.Context) error {
	service := &m.EIPService{
		Locations:     r.locations,
		OpenVPNConfig: r.openvpnConfig,
	}
	return c.JSON(http.StatusOK, service)
}
