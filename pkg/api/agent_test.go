package api

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	m "0xacab.org/leap/menshen/pkg/models"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestRegisterBridge(t *testing.T) {

	location1 := "New York"
	bridge1 := &m.Bridge{
		Host:      "bridge1",
		Port:      443,
		Transport: "TCP",
		Type:      "obfs4",
		Bucket:    "bucket1",
		Location:  location1,
		Options: map[string]any{
			"cert":    "test-cert1",
			"iatMode": "test-iat1",
		},
	}
	bridge2 := &m.Bridge{
		Host:      "bridge2",
		Port:      443,
		Transport: "TCP",
		Type:      "obfs4",
		Bucket:    "bucket1",
		Location:  location1,
		Options: map[string]any{
			"cert":    "test-cert2",
			"iatMode": "test-iat2",
		},
	}

	type testState struct {
		mockRegistry    *registry
		bridgeRequest   m.Bridge
		evaluateResults func(t *testing.T, rec *httptest.ResponseRecorder, reg *registry)
	}

	testTable := []struct {
		name     string
		genState func() testState
	}{
		{"Add a bridge",
			func() testState {
				timePreTest := time.Now().UnixMilli()
				return testState{
					&registry{
						bridges: bridgeMap{location1: []*m.Bridge{bridge1}},
					},
					*bridge2,
					func(t *testing.T, rec *httptest.ResponseRecorder, reg *registry) {
						assert.Equal(t, http.StatusCreated, rec.Code)

						assert.Equal(t, 2, len(reg.bridges[location1]))
						actualBridge1 := reg.bridges[location1][0]
						actualBridge2 := reg.bridges[location1][1]

						assert.True(t, bridgesMatch(*bridge1, *actualBridge1), "bridge1 in actual registry should match expected")
						assert.True(t, bridgesMatch(*bridge2, *actualBridge2), "bridge2 in actual registry should match expected")

						// Test that bridge2 LastSeenMillis is within range
						timePostTest := time.Now().UnixMilli()
						assert.True(t, actualBridge2.LastSeenMillis >= timePreTest && actualBridge2.LastSeenMillis <= timePostTest, "LastSeenMillis should be properly updated")
					},
				}
			},
		},
		{"Update a bridge's LastSeenMillis",
			func() testState {
				timePreTest := time.Now().UnixMilli()
				return testState{
					&registry{
						bridges: bridgeMap{location1: []*m.Bridge{bridge1}},
					},
					*bridge1,
					func(t *testing.T, rec *httptest.ResponseRecorder, reg *registry) {
						assert.Equal(t, http.StatusOK, rec.Code)

						assert.Equal(t, 1, len(reg.bridges[location1]))
						actualBridge1 := reg.bridges[location1][0]

						assert.True(t, bridgesMatch(*bridge1, *actualBridge1), "bridge1 in actual registry should match expected")

						// Test that bridge1 LastSeenMillis is within range
						timePostTest := time.Now().UnixMilli()
						assert.True(t, actualBridge1.LastSeenMillis >= timePreTest && actualBridge1.LastSeenMillis <= timePostTest, "LastSeenMillis should be properly updated")
					},
				}
			},
		},
	}

	for _, tc := range testTable {
		t.Run(tc.name, func(t *testing.T) {

			e := echo.New()
			ts := tc.genState()
			agentEndpoints := e.Group("/api/5/agent")
			agentEndpoints.PUT("/bridge", ts.mockRegistry.RegisterBridge)

			requestBody, err := json.Marshal(ts.bridgeRequest)
			assert.NoError(t, err)
			req := httptest.NewRequest(http.MethodPut, "/api/5/agent/bridge", bytes.NewReader(requestBody))
			req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
			rec := httptest.NewRecorder()

			e.ServeHTTP(rec, req)

			ts.evaluateResults(t, rec, ts.mockRegistry)

			// Sleep one millisecond for more realistic results re: timing
			time.Sleep(time.Millisecond)

		})
	}
}

func TestRegisterGateway(t *testing.T) {

	location1 := "New York"
	gateway1 := &m.Gateway{
		Location: location1,
		Type:     "openvpn",
		Bucket:   "bucket1",
	}

	gateway2 := &m.Gateway{
		Location: location1,
		Type:     "openvpn",
	}

	type testState struct {
		mockRegistry    *registry
		gatewayRequest  m.Gateway
		evaluateResults func(t *testing.T, rec *httptest.ResponseRecorder, reg *registry)
	}

	testTable := []struct {
		name     string
		genState func() testState
	}{
		{"Add a gateway",
			func() testState {
				timePreTest := time.Now().UnixMilli()
				return testState{
					&registry{
						gateways: gatewayMap{location1: []*m.Gateway{gateway1}},
					},
					*gateway2,
					func(t *testing.T, rec *httptest.ResponseRecorder, reg *registry) {
						assert.Equal(t, http.StatusCreated, rec.Code)

						assert.Equal(t, 2, len(reg.gateways[location1]))
						actualGateway1 := reg.gateways[location1][0]
						actualGateway2 := reg.gateways[location1][1]

						assert.True(t, gatewaysMatch(*gateway1, *actualGateway1), "gateway1 in actual registry should match expected")
						assert.True(t, gatewaysMatch(*gateway2, *actualGateway2), "gateway2 in actual registry should match expected")

						// Test that bridge2 LastSeenMillis is within range
						timePostTest := time.Now().UnixMilli()
						assert.True(t, actualGateway2.LastSeenMillis >= timePreTest && actualGateway2.LastSeenMillis <= timePostTest, "LastSeenMillis should be properly updated")
					},
				}
			},
		},
		{"Update a gateway's LastSeenMillis",
			func() testState {
				timePreTest := time.Now().UnixMilli()
				return testState{
					&registry{
						gateways: gatewayMap{location1: []*m.Gateway{gateway1}},
					},
					*gateway1,
					func(t *testing.T, rec *httptest.ResponseRecorder, reg *registry) {
						assert.Equal(t, http.StatusOK, rec.Code)

						assert.Equal(t, 1, len(reg.gateways[location1]))
						actualGateway1 := reg.gateways[location1][0]

						assert.True(t, gatewaysMatch(*gateway1, *actualGateway1), "gateway1 in actual registry should match expected")

						// Test that gateway1 LastSeenMillis is within range
						timePostTest := time.Now().UnixMilli()
						assert.True(t, actualGateway1.LastSeenMillis >= timePreTest && actualGateway1.LastSeenMillis <= timePostTest, "LastSeenMillis should be properly updated")
					},
				}
			},
		},
	}

	for _, tc := range testTable {
		t.Run(tc.name, func(t *testing.T) {

			e := echo.New()

			ts := tc.genState()
			agentEndpoints := e.Group("/api/5/agent")
			agentEndpoints.PUT("/gateway", ts.mockRegistry.RegisterGateway)

			requestBody, err := json.Marshal(ts.gatewayRequest)
			assert.NoError(t, err)
			req := httptest.NewRequest(http.MethodPut, "/api/5/agent/gateway", bytes.NewReader(requestBody))
			req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
			rec := httptest.NewRecorder()

			e.ServeHTTP(rec, req)

			ts.evaluateResults(t, rec, ts.mockRegistry)

			// Sleep one millisecond for more realistic results re: timing
			time.Sleep(time.Millisecond)

		})
	}

}
