package geolocate

import (
	"testing"
)

func TestGetRegionForCountryCode(t *testing.T) {
	type args struct {
		cc string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "hu is north",
			args: args{"hu"},
			want: North,
		},
		{
			name: "VG is south",
			args: args{"VG"},
			want: South,
		},
		{
			name: "GL is north",
			args: args{"GL"},
			want: North,
		},
		{
			name: "KI is south",
			args: args{"VG"},
			want: South,
		},
		{
			name: "XX is unknown",
			args: args{"XX"},
			want: UnknownRegion,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetRegionForCountryCode(tt.args.cc); got != tt.want {
				t.Errorf("GetRegionForCountryCode() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetContinentForCountryCode(t *testing.T) {
	type args struct {
		cc string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "AF is asia",
			args: args{"AF"},
			want: AS,
		},
		{
			name: "af is asia",
			args: args{"af"},
			want: AS,
		},
		{
			name: "KY is north america",
			args: args{"KY"},
			want: NA,
		},
		{
			name: "ML is africa",
			args: args{"ML"},
			want: AF,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetContinentForCountryCode(tt.args.cc); got != tt.want {
				t.Errorf("GetContinentForCountryCode() = %v, want %v", got, tt.want)
			}
		})
	}
}
