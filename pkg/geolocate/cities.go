package geolocate

import (
	"0xacab.org/leap/menshen/pkg/models"
	"github.com/tidwall/cities"
)

// GeolocateCities receives an array of strings, and it returns
// a map[string]Point
func GeolocateCities(target []string) map[string]Point {
	m := make(map[string]Point, 0)
	for _, c := range cities.Cities {
		if hasElement(target, models.CanonicalizeLocation(c.City)) {
			m[models.CanonicalizeLocation(c.City)] = Point{c.Latitude, c.Longitude}
		}
	}
	return m
}

func hasElement(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}
	return false
}
