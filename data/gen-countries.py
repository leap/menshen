#!/usr/bin/env python3
#
# This script converts the `countries.csv` original dataset
# into a map[string]*point that can be used from Go.
#
import csv

data = {}


def parseData():
    with open('countries.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')
        for row in reader:
            data[row["country_code"]] = row["latitude"], row["longitude"]

pre = """// auto-generated code. Do NOT edit.
package geolocate

type Point struct {
    Lat float32
    Lon float32
}

var centroids = map[string]*Point {"""

def genGoSrc():
    print(pre)
    for cc, coords in data.items():
          lon, lat = coords[0], coords[1]
          if lon == '' or lat == '':
              continue
          print(f'\t"{cc}": {{{lon}, {lat}}},')
    print('}')


if __name__ == "__main__":
    parseData()
    genGoSrc()
